def fun_sales(spark):
    '''
    @
    '''
    v_sql = \
    """
    select name
    , year
    , percentage
    from 
        sales 
    """
    df=spark.sql(v_sql)
    df.createOrReplaceTempView("sales")
    spark.catalog.cacheTable("sales")
    return df
    