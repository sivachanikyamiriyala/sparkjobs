import os
import sys 

import pyspark

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession, HiveContext, SQLContext
from pyspark.sql import functions 
from pyspark.sql.functions import *
from pyspark.sql.column import Column 

from sales import fun_sales
from products import fun_products
from student_sample_partition import fun_student_sample_partition

conf=SparkConf()
conf.set("spark.app.name","appname")
conf.set("spark.sql.crossJoin.enabled","true")
conf.set("spark.sql.partitionOverwriteMode","dynamic")
conf.set("hive.exec.dynamic.partition","true")
conf.set("hive.exec.dynamic.partition.mode","nonstrict")

v_mis_dt='2021-09-09'
database_name="chanikya"

spark=SparkSession.builder.config(conf=conf).enableHiveSupport().getOrCreate()
#spark.catalog.setCurrentDatabase(database_name)

if __name__ == "__main__":
    df_student_sample_partition=fun_student_sample_partition(spark=spark,v_mis_dt=v_mis_dt).distinct().repartition(2,'student_name').cache()
    df_student_sample_partition.show()
    #df_sales=fun_sales(spark=spark).distinct().repartition(2,'name').cache()
    #df_products=fun_products(spark=spark).distinct().repartition(2,'name').cache()
    #df_join=df_sales.join(df_products,df_sales.name==df_products.name,'left_outer').distinct().cache()
    #df_join.show()
    
spark.stop()
sys.exit(0)
