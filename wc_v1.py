# Importing python modules
import os 
import sys
import dateutil
import json
import requests 
import re 
import requests 

#Importing spark libraries 
from pyspark.sql import SparkSession
sparkdriver=SparkSession.builder.master('local').appName('sampleapplication').getOrCreate()
r1=sparkdriver.sparkContext.textFile(sys.argv[1])
r2=r1.map(lambda x:x.encode('utf-8'))
r3=r2.flatMap(lambda x:x.split(" ")).map(lambda x:(x,1)).reduceByKey(lambda x,y:x+y).sortByKey(x:x,False)
r3.saveAsTextFile(sys.argv[2])
sparkdriver.stop()