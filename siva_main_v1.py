import os
import sys 
from dateutil.parser import parse 
from platform import python_version 

import pyspark

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession, HiveContext, SQLContext
from pyspark.sql.column import Column
from pyspark.sql.types import *

from siva_sales import fun_sales
from siva_products import fun_products
from student_sample_partition import fun_student_sample_partition

conf = SparkConf()
conf.set("spark.app.name","sparknameapplication")
conf.set("spark.sql.partitionOverwriteMode","dynamic")
conf.set("spark.sql.crossJoin.enabled","true")
conf.set("spark.exec.dynamic.partition","true")
conf.set("spark.exec.dynamic.partition.mode","nonstrict")

spark = SparkSession.builder.config(conf=conf).enableHiveSupport().getOrCreate()

sc=spark._sc
sqlContext=SQLContext(sc)

v_mis_dt ='2021-09-09'
if __name__ == "__main__":
    df_student_sample_partition = fun_student_sample_partition(spark=spark,v_mis_dt=v_mis_dt).distinct().repartition(2,"student_name").cache()
    df_student_sample_partition.show()
    df_sales = fun_sales(spark=spark).distinct().repartition(2,'name').cache()
    df_products = fun_products(spark=spark).distinct().repartition(2,'name').cache()
    df_join=df_sales.join(df_products,df_sales.name==df_products,'left')
    df_join.show()
    
spark.catalog.clearCache()
spark.stop()
sys.exit(0)
    
    