def fun_student_sample_partition(spark,v_mis_dt):
    '''
    @
    '''
    v_sql = \
    """
    select student_name
    , student_id
    , student_course
    , studdent_year
    , case student_name is not NULL and student_name <> '' and student_name <> ' ' then 'RAJU' else student_name end as case_statement
    from 
    chanikya.student_sample_partition 
    where student_date like '{0}'
    """.format(v_mis_dt)
    df = spark.sql(v_sql)
    df.createOrReplaceTempView("student_sample_partition")
    spark.catalog.cacheTable("student_sample_partition")
    return df
    