import os
import sys

from dateutil.parser import parse
from platform import python_version

import pyspark

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession, HiveContext, SQLContext

from pyspark.sql.column import Column
from pyspark.sql.types import *

conf = SparkConf()
conf.set("spark.app.name","sampleapplication")
conf.set("spark.sql.crossJoin.enabled","true")
conf.set("spark.sql.partitionOverwriteMode","dynamic")
conf.set("hive.exec.dynamic.partition","true")
conf.set("hive.exec.dynamic.partition.mode","nonstrict")

v_mis_dt = sys.argv[1]

spark = SparkSession.builder.config(conf=conf).enableHiveSupport().getOrCreate()

if __name__ == "__main__":

    df_student_sample_partition = fun_student_sample_partition(spark=spark,v_mis_dt=v_mis_dt).distinct().repartition(2,'name').cache()
    df_student_sample_partition.show()

spark.stop()

sys.exit(0)

