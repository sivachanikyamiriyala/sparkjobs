def fun_products(spark):
    '''
    @
    '''
    v_sql = \
    """
    select 
    , name
    , id
    , price 
    from chanikya.products 
    """
    df=spark.sql(v_sql)
    df.createOrReplaceTempView("products")
    spark.catalog.cacheTable("products")
    return df
    
    