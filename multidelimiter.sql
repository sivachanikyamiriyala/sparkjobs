use chandra ;

create table if not exists `table1`
(`line` string comment 'line')
row format delimited 
;


load data local inpath '/home/sivachanikyamiriyala3826/hive_chandra/multidelimiterfile' overwrite into table table1 ;

select * from table1 ;

create table table2 as 
select substr(line,1,3)
       ,substr(line,5,7)
	   ,substr(line,9,10)
	   from table1 ;
	   
select * from table1 ;
select * from table2 ;
	   