import os 
import sys 

from dateutil.parser import parse 
from platform import python_version

import pyspark
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession, HiveContext, SQLContext
from pyspark.sql.functions import *
from pyspark.sql.column import Column

from sales import fun_sales
from products import fun_products
from student_sample_partition import fun_student_sample_partition
from join import join

conf=SparkConf()
conf.set("spark.app.name","sparkapplicationname")
conf.set("spark.sql.crossJoin.enabled","true")
conf.set("spark.sql.partitionOverwriteMode","dynamic")
conf.set("hive.exec.partition","true")
conf.set("hive.exec.partition.mode","nonstrict")
conf.set("spark.ui.enabled","true")

database="chanikya"

v_mis_dt=sys.argv[1]

spark=SparkSession.builder.config(conf=conf).enableHiveSupport().getOrCreate()
sc=spark._sc
sqlC=SQLContext(sc)

spark.catalog.setCurrentDatabase(database)

if __name == "__main__":
    df_student_sample_partition=fun_student_sample_partition(spark=spark,v_mis_dt=v_mis_dt).distinct().repartition(2,'student_name').cache()
    df_sales=fun_sales(spark=spark).distinct().repartition(2,'name').cache()
    df_products=fun_products(spark=spark).distinct().repartition(2,'name').cache()
    df_student_sample_partition.show(2)
    df_sales.show(2)
    df_products.show(2)
    
spark.catalog.clearCache()    
spark.stop()    
