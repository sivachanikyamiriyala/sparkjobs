create database if not exists gcgtwasg
comment 'database name is gcgtwasg'
location '/user/sivachanikyamiriyala3826/gcgtwasg' 
with dbproperties('key1'='value1','key2'='value2')

;

use gcgtwasg ;

create external table if not exists `STUDENT1`
(
`student_name` string comment 'student name',
`student_id` int comment 'student id',
`student_course` string comment 'student course',
`student_year` int comment 'student year'
)
comment 'student1 is the table name'
row format delimited 
fields terminated by '\t'
lines terminated by '\n'
stored as textfile 
location '/user/sivachanikyamiriyala3826/gcgtwasg/STUDENT1' 
;

create external table if not exists `STUDENT2`
(
`student_name` string comment 'student name',
`student_id` int comment 'student id',
`student_course` string comment 'student course',
`student_year` int comment 'student year'
)
comment 'student2 is the table name'
row format delimited 
fields terminated by '\t'
lines terminated by '\n'
stored as textfile 
location '/user/sivachanikyamiriyala3826/gcgtwasg/STUDENT2' 
;

create external table if not exists `STUDENT3`
(
`student_name` string comment 'student name',
`student_id` int comment 'student id',
`student_course` string comment 'student course',
`student_year` int comment 'student year'
)
comment 'student3 is the table name'
row format delimited 
fields terminated by '\t'
lines terminated by '\n'
stored as textfile 
location '/user/sivachanikyamiriyala3826/gcgtwasg/STUDENT3' 
;

create temporary table if not exists `STUDENT4`
(
`student_name` string comment 'student name',
`student_id` int comment 'student id',
`student_course` string comment 'student course',
`student_year` int comment 'student year'
)
comment 'student4 is the table name'
row format delimited 
fields terminated by '\t'
lines terminated by '\n'
stored as textfile 
;

load data local inpath '/home/sivachanikyamiriyala3826/student.txt' overwrite into table STUDENT1 ;

load data local inpath '/home/sivachanikyamiriyala3826/student.txt' overwrite into table STUDENT2 ;

load data inpath '/user/sivachanikyamiriyala3826/gcgtwasg/STUDENT2/student.txt' overwrite into table STUDENT1 ;

select * from STUDENT1 ;

SELECT * FROM STUDENT2 ;

load data inpath '/user/sivachanikyamiriyala3826/gcgtwasg/STUDENT2/student.txt' into table STUDENT1 ;

select * from STUDENT1 ;

hdfs dfs -mkdir sunday_practice_1 ;

hdfs dfs -put student.txt sunday_practice_1 ;

create table if not exists `STUDENT_TEXT1`
(
`STUDENT_NAME` string comment 'STUDENT NAME',
`STUDENT_ID` int comment 'student id',
`STUDENT_COURSE` string comment 'student course',
`STUDENT_YEAR` int comment 'student year'
)
comment 'student_text1 is the table name'
row format delimited
fields terminated by '\t' 
lines terminated by '\n' 
stored as textfile 
;


create external table if not exists `STUDENT_TEXT2`
(
`STUDENT_NAME` string comment 'STUDENT NAME',
`STUDENT_ID` int comment 'student id',
`STUDENT_COURSE` string comment 'student course',
`STUDENT_YEAR` int comment 'student year'
)
comment 'student_text2 is the table name'
row format delimited
fields terminated by '\t' 
lines terminated by '\n' 
stored as textfile 
location '/user/sivachanikyamiriyala3826/sunday_practice_1' 
;

select * from STUDENT_TEXT1;

select * from STUDENT_TEXT2;

create external table if not exists `STUDENT` 
(
`student_name` string comment 'student name',
`student_id` int comment 'student id',
`student_course` string comment 'student course',
`student_year` int comment 'student year'
)
comment 'student is the table name'
row format delimited
fields terminated by '\t' 
lines terminated by '\n' 
stored as textfile 
location '/user/sivachanikyamiriyala3826/gcgtwasg/STUDENT'
;

insert overwrite table student select * from student3 ;

select * from student ;






