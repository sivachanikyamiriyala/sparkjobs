import os 
import sys 
import pyspark

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession, HiveContext, SQLContext
from pyspark.sql.column import Column
from pyspark.sql.functions import *

conf=SparkConf()
conf.set("spark.app.name","appnamesample")
conf.set("spark.sql.crossJoin.enabled","true")
conf.set("hive.exec.dynamic.partition","true")
conf.set("hive.exec.dynamic.partition.mode","nonstrict")
conf.set("spark.sql.partitionOverwriteMode","dynamic")

spark=SparkSession.builder.config(conf=conf).enableHiveSupport().getOrCreate()
v_mis_dt ='2021-09-09'
if __name__=="__main__":
    df_student_sample_partition=fun_student_sample_partition(spark=spark,v_mis_dt=v_mis_dt).distinct().repartition(2,'student_name').cache()
    df_student_sample_partition.show()
    
 spark.stop()   
    