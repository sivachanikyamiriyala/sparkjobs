import os 
import sys 
from platform import python_version

import pyspark
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession, HiveContext, SQLContext
from pyspark.sql.functions import *
from pyspark.sql.column import Column

from file11 import fun_student_sample_partition

conf=SparkConf()
conf.set("spark.app.name","student_sample_partition")
conf.set("spark.sql.crossJoin.enabled","true")
conf.set("spark.sql.partitionOverwriteMode","dynamic")
conf.set("hive.exec.dynamic.partition","true")
conf.set("hive.exec.dynamic.partition.mode","nonstrict")

spark = SparkSession.builder.config(conf=conf).enableHiveSupport().getOrCreate()
sc = spark._sc

sqlContext = SQLContext(sc)
v_mis_dt = '2021-09-09'
df_student_sample_partition = fun_student_sample_partition(spark=spark,v_mis_dt=v_mis_dt).distinct().repartition(2,"student_name").cache()

df_student_sample_partition.show()
